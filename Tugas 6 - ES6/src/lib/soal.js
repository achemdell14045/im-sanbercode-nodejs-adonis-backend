//soal no 1
export const sapa = (nama) => {
    return `halo, selamat pagi ${nama}`
}

//soal no 2
export const convert = (nama, domisili, umur) => {
    return {
        nama,
        domisili,
        umur
    }
}

//soal no 3
export const checkScore = (stringData) => {
    let cek = stringData[0].split(",")
    const turu = [];
    for (let p = 0; p < cek.length; p++) {
        
        turu [cek[p].split(":")[0]] = cek[p].split(":")[1];
    }
    console.log(turu)
}

// soal no 4

const data = [
    { name: "Ahmad", kelas: "adonis"},
    { name: "Regi", kelas: "laravel"},
    { name: "Bondra", kelas: "adonis"},
    { name: "Iqbal", kelas: "vuejs" },
    { name: "Putri", kelas: "Laravel" }
]


export const filterData = (kelas) => {
    for (let i = 0; i < kelas.length; i++) {

    return data.filter((el) => el[`kelas`].toLowerCase().includes(kelas.toLowerCase()));
    }
}
import {sapa, convert, checkScore, filterData} from "./lib/soal";

const myArg = process.argv.slice(2);
const params = myArg.slice(1);
const command = myArg[0]

switch (command) {
    case "sapa":
        console.log(sapa(myArg[1]));
        break;

    case "convert":
        let [nama, domisili, umur] = params

        console.log(convert(nama, domisili, umur))
        break;
    
    case "checkScore":
        console.log(checkScore(myArg.slice(1)))
        break;


    case "filterData":
        let kelas = myArg[1]
        console.log(filterData(kelas));
        break;
    default:
        break;
}
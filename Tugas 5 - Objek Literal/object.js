
// soal no.1

function arrayToObject(arr) {
    for(var i = 0; i < arr.length; i++) {
        
        var tahun = (new Date()).getFullYear();
        var person = arr[i];

        var objPerson = {
            firstname: person[0],
            lastname: person[1],
            gender: person[2],
        }

        if (!person[3] || person[3] > tahun) {
            objPerson.age = "Invalid Birth Year"
        } else {
            objPerson.age = tahun - person[3]
        }

        var fullName = objPerson.firstname + " " + objPerson.lastname;
        console.log(`${i + 1} . ${fullName} : ` , objPerson)
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)


arrayToObject([]);



// Soal No.2

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    
    var output = []
    for(var t = 0; t < arrPenumpang.length; t++) {
        
        var penumpangSekarang = arrPenumpang[t]
        var obj = {
            penumpang: penumpangSekarang[0],
            naikDari: penumpangSekarang[1],
            tujuan: penumpangSekarang[2],
        }
        var bayar = (rute.indexOf(penumpangSekarang[2]) - rute.indexOf(penumpangSekarang[1])) * 2000

        obj.bayar = bayar
        output.push(obj)
    }
    return output
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]




  // Soal No.3
  function nilaiTertinggi(siswa) {
    var result = []
    for (var p = 0; p < siswa.length; p++) {
        var data;
        if (result.hasOwnProperty(siswa[p].class)) {
            var kelas = siswa[p].class
            if(result[kelas].score < siswa[p].score){
                result[kelas].name = siswa[p].name
                result[kelas].score = siswa[p].score
            }
        }else{
            var kelas = siswa[p].class
            result = {
                ...result,
                [kelas]: {
                    name: siswa[p].name,
                    score: siswa[p].score
                }
            }
        }

    }
    return result
}
  
  // TEST CASE
  console.log(nilaiTertinggi([
    {
      name: 'Asep',
      score: 90,
      class: 'adonis'
    },
    {
      name: 'Ahmad',
      score: 85,
      class: 'vuejs'
    },
    {
      name: 'Regi',
      score: 74,
      class: 'adonis'
    },
    {
      name: 'Afrida',
      score: 78,
      class: 'reactjs'
    }
  ]));
  
  // OUTPUT:
  
  // {
  //   adonis: { name: 'Asep', score: 90 },
  //   vuejs: { name: 'Ahmad', score: 85 },
  //   reactjs: { name: 'Afrida', score: 78}
  // }
  
  
  console.log(nilaiTertinggi([
    {
      name: 'Bondra',
      score: 100,
      class: 'adonis'
    },
    {
      name: 'Putri',
      score: 76,
      class: 'laravel'
    },
    {
      name: 'Iqbal',
      score: 92,
      class: 'adonis'
    },
    {
      name: 'Tyar',
      score: 71,
      class: 'laravel'
    },
    {
      name: 'Hilmy',
      score: 80,
      class: 'vuejs'
    }
  ]));
  
  // {
  //   adonis: { name: 'Bondra', score: 100 },
  //   laravel: { name: 'Putri', score: 76 },
  //   vuejs: { name: 'Hilmy', score: 80 }
  // }
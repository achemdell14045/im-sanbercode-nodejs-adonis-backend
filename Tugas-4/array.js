// Soal No.1
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHeanding(n) {
    for (h = 0; h < n.length; h++) {
        console.log("Nomor ID: " + n[h][0]);
        console.log("Nama Lengkap: " + n[h][1]);
        console.log("TTL: " + n[h][2] + " " + n[h][3]);
        console.log("Hobi: " + n[h][4]);
        console.log("");
    }
}

console.log(dataHeanding(input));


// Soal No.2
console.log("Soal No.2");

function balikKata(parameter) {
    var i = "";
    for (var p = parameter.length-1; p >= 0; p--) {
        i += parameter[p];
    }
    return i 
}

console.log(balikKata("SanberCode")) 
// Output: edoCrebnaS

console.log(balikKata("racecar")) 
// Output: racecar

console.log(balikKata("kasur rusak"))
// Output: kasur rusak

console.log(balikKata("haji ijah"))
// Output: haji ijah

console.log(balikKata("I am Sanbers"))
// Output: srebnaS ma I
// if-esle

var nama = "John";
var peran = "";

if (nama == "") {
    console.log("John")
    if (peran ="")
    console.log("penyihir")
} else {
    console.log("Nama harus diisi!")
}


if (nama == "wahyu") {
    console.log("Hallo John,")
} else {
    console.log("Hallo John, Pilih peranmu untuk memulai game!")
}



if (nama == "John"){
    console.log("Selamat datang di Dunia Werewolf, Jane")
    if (peran == "") {
        console.log("Hallo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf !")
    } else if (peran == "Guard"){
        console.log("peran Guard")
    }
} else {
    console.log("Hayo siapa yang melihat yang menjadi werewolf?")
}


if (nama == "John") {
    console.log("Selamat datang di Dunia werewolf, Jenita")
    if (peran == "") {
        console.log("Hallo Guard jenita, kamu akan melindungi temanmu dari serangan werewolf.")
    } else if (peran == "werewolf") {
        console.log("peran werewolf")
    }
} else {
    console.log("saya akan siap melindungi teman saya")
}


if (nama == "John") {
    console.log("Selamat datang di Dunia Werewolf, Junaedi")
    if (peran == "") {
        console.log("Hallo Werewolf Junaedi, kamu akan memakan mangsa setiap malam !")
    } else if (peran == "penyihir") {
        console.log("akan di mangsa")
    }
} else {
    console.log(" saya akan memangsa setiap saat")
}



// switch case 

var hari = 21; 
var bulan = 1; 
var tahun = 1945;

var hariIni = (1 >= hari <= 31) ? hari : "Salah hari";  // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulanPuasa = (1 >= bulan <= 12) ? bulan : "Salah bulan";  // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahunMasehi = (1900 >= tahun <= 2200) ? tahun : "Salah tahun";  // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

function bulanPurnama (x) {
  switch(bulanPuasa) {
    case 1:   { bulan = "Januari"; break; }
    case 2:   { bulan = "Ferbuari"; break; }
    case 3:   { bulan = "Maret"; break; }
    case 4:   { bulan = "April"; break; }
    case 5:   { bulan = "Mei"; break; }
    case 6:   { bulan = "Juni"; break; }
    case 7:   { bulan = "Juli"; break; }
    case 8:   { bulan = "Agustus"; break; }
    case 9:   { bulan = "September"; break; }
    case 10:  { bulan = "Oktober"; break; }
    case 11:  { bulan = "November"; break; }
    case 12:  { bulan = "Desember"; break; }
    default:  { console.log('Tidak ada nama bulan'); }}
}
var bulanSabit = bulanPurnama(bulanPuasa)

//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 
console.log(hariIni, bulan, tahunMasehi)
console.log("Soal No.1")

console.log( "Looping Pertama");

var i = 2;
while (i <= 21) {
    var output = i + " - Love Coding";
    console.log(output);
    i += 2;
}

console.log("Looping Kedua");

var f = 20;
while(f >= 2) {
    var output = f + " - I will become a mobile developer";
    console.log(output);
    f -= 2;
}


console.log("No.2");

for (var p = 1; p <= 20; p++) {
    var tampung1 = "";
    if (p % 2 == 1) {
        if (p % 3 == 0) {
            tampung1 = p + "- i Love Coding";
        } else {
            tampung1 = p + "- Santai";
        }
    } else {
        tampung1 = p + "- Berkualitas"
    }
    console.log(tampung1);
}

console.log("Soal No.3");

function makeReactangle(panjang, lebar) {
    for (var p = 1; p <= lebar; p++) {
        var baris1 = "";
        for (var j = 1; j <= panjang; j++) {
            baris1 += "#";
        }
        console.log(baris1);
    }
}

makeReactangle(8,4);


console.log("Soal No.4");

function makeLeader(sisi) {
    for (r = 0; r <= sisi; r++) {
        baris = "";
        for (var t = 0; t <= r; t++) {
            baris += "#";
        }
        console.log(baris);
    }
}

makeLeader(7)